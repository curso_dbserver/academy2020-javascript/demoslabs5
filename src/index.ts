import { connect, Schema, model } from 'mongoose';

async function main() {
    try {
        const url = 'mongodb://localhost:27017/testemongoose';
        const cliente = await connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('Conectado com sucesso');

        //Definir um schema
        const pessoaSchema = new Schema({ //tem que importar schema
            nome: { type: String, required: true, minlength: 1, maxlength: 50 },  //required - obrigatorio
            idade: { type: Number, required: true, min: 0 }
        });

        //Definir um modelo 
        const pessoaModel = model('Pessoa', pessoaSchema, 'pessoas'); //pessoas - coleção 

        //Inserir um documento
        /*
        const pessoaDocument = new pessoaModel({nome: 'Rose', idade: 30});
        const pessoaDocumentInserido = await pessoaDocument.save();  
        console.log('Inserido: ');
        console.log(pessoaDocumentInserido);
        */

        /*
        //Consultar um documento 
        const consulta = pessoaModel.find().where('idade').lt(30).sort('nome'); //sort - ordenadas por 'nome'
        const resultado = await consulta.exec();
        console.log('Busca');
        console.log(resultado);
        */

        /*
        //Alterar um documento
        const umaPessoaDocument = await pessoaModel.findById('5f2ba1185b1dbe49a4ef2ef2').exec();
        console.log('Busca:');
        console.log(umaPessoaDocument);
        umaPessoaDocument!.set('idade', 24);
        const umaPessoaDocumentAlterada = await umaPessoaDocument?.save();
        console.log('Alterado:');
        console.log(umaPessoaDocumentAlterada);
        */

        //Remover documento 
        const umaPessoaDocument = await pessoaModel.findById('5f2ba1185b1dbe49a4ef2ef2').exec();
        const umaPessoaDocumentRemoved =  await umaPessoaDocument?.remove();
        console.log('Removido: ');
        console.log(umaPessoaDocumentRemoved);
       

        if (cliente && cliente.connection) {
            await cliente.connection.close();
            console.log('Desconectado');
        }
    } catch (erro) {
        console.log('Erro de acesso ao BD:');
        console.log(erro);
    }
}
main();